#include <iostream>
#include <func.h>

int main() {
    std::string name;
    std::cin >> name;
    std::cout << func(name);
    return 0;
}