cmake_minimum_required(VERSION 3.8)

project(test_ci)

set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

include_directories("include/")

add_subdirectory("libs/googletest" "googletest")

add_library(func STATIC libs/func/func.cpp)

add_executable(main src/main.cpp)

add_executable(test tests/test_t.cpp)

set(CMAKE_CXX_FLAGS "-O0 -fprofile-arcs -ftest-coverage")

target_link_libraries(test gtest gtest_main func gcov)

target_link_libraries(main func gcov)
