```plantuml

@startuml

User -> EventDispatcher : Query
activate EventDispatcher
|||
EventDispatcher -> ConsoleApp: Query
deactivate EventDispatcher
activate ConsoleApp
|||
ConsoleApp -> JSONSerializer: Query
activate JSONSerializer
|||
JSONSerializer -> ConsoleApp: deserialized Query
deactivate JSONSerializer
ConsoleApp -> QueryProccesor: deserialized Query
activate QueryProccesor
|||
QueryProccesor -> ConsoleApp: return code
deactivate QueryProccesor
ConsoleApp -> EventDispatcher: return code
deactivate ConsoleApp




@enduml

```
